package co.org.invemar.sigma.porlets;

import java.util.Map;



public interface Command {
     public String execute();
}
