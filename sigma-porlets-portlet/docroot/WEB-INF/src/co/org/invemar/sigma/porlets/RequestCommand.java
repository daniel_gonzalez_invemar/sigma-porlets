package co.org.invemar.sigma.porlets;

import java.util.Map;

public class RequestCommand {
    private Map<String, String>  parameters;

	/**
	 * @param parameters
	 */
	public RequestCommand(Map<String, String> parameters) {
		super();
		this.parameters = parameters;
	}	


	/**
	 * @return the parameters
	 */
	public Map<String, String> getParameters() {
		return parameters;
	}

	/**
	 * @param parameters the parameters to set
	 */
	public void setParameters(Map<String, String> parameters) {
		this.parameters = parameters;
	}
    
}
