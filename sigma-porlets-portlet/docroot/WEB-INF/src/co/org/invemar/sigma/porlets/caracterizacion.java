package co.org.invemar.sigma.porlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import co.org.invemar.sigma.porlets.persistence.StudioCase;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;




/**
 * Portlet implementation class caracterizacion
 */
public class caracterizacion extends GenericPortlet {

	

	public void init() {
		editTemplate = getInitParameter("edit-template");
		viewTemplate = getInitParameter("view-template");
		
	}


	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {

		super.processAction(actionRequest, actionResponse);
	}

	public void serveResource(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {

		super.serveResource(request, response);
		
		
		String userDB =  request.getPortletSession().getPortletContext().getInitParameter("usermanglar");
		String pwdDB = request.getPortletSession().getPortletContext().getInitParameter("pwdmanglar");
		
		
		response.setContentType("text/json");
		PrintWriter writer = response.getWriter();
		
		
		String codeStudioCase = ParamUtil.getString(request, "codStudioCase");
		String action = ParamUtil.getString(request, "action");		
		
		  
		
		if (action.equalsIgnoreCase("FINDAREA")){
		
			
			
			  HashMap<String,String> parameters = new HashMap<String,String>();
			  parameters.put("codStudioCase", codeStudioCase);
			  parameters.put("userDB", userDB);
			  parameters.put("pwdDB", pwdDB);
			  
			  RequestCommand requestCommand = new RequestCommand(parameters);
			  
			  FindAreaCommand findAreaCommand = new FindAreaCommand(requestCommand);
			  String jsonString = findAreaCommand.execute();
			 // System.out.println("json string:"+jsonString);
			  writer.print(jsonString);	
			  writer.flush();
			  writer.close();
			 
		}
		
		
		
	}

	public void doEdit(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		include(editTemplate, renderRequest, renderResponse);
	}

	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		include(viewTemplate, renderRequest, renderResponse);
		
		
		
		
		
		
	}

	protected void include(String path, RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		PortletRequestDispatcher portletRequestDispatcher = getPortletContext()
				.getRequestDispatcher(path);

		if (portletRequestDispatcher == null) {
			_log.error(path + " is not a valid include");
		} else {
			portletRequestDispatcher.include(renderRequest, renderResponse);
		}
	}

	protected String editTemplate;
	protected String viewTemplate;

	private static Log _log = LogFactoryUtil.getLog(caracterizacion.class);

}
