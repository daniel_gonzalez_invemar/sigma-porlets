package co.org.invemar.sigma.porlets.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;


public class StudioCase {	

		
	public JSONObject getAllCaseStudioByType(int caseStudioType,Connection con) {
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
	
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String query = "select  * from areasbosques where tipo=?  order by id desc";
		try {
           
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, caseStudioType);
			//System.out.println("query:"+query);
			rs = pstmt.executeQuery();
			JSONArray jsonarrayData = JSONFactoryUtil.createJSONArray();
			while (rs.next()) {
				JSONObject jo = JSONFactoryUtil.createJSONObject();
				
				jo.put("nombre", rs.getString("nombre"));
				jo.put("nombresector", rs.getString("nombresector"));
				jo.put("nombrecorto", rs.getString("nombrecorto"));
				jo.put("id", rs.getString("id"));
				jo.put("id1", rs.getString("id1"));
				jo.put("tipo", rs.getString("tipo"));
				jo.put("nombreregion", rs.getString("nombreregion"));
				jo.put("zonaprotegida", rs.getString("zonaprotegida"));
				jo.put("cuencahidrografica", rs.getString("cuencahidrografica"));
				jo.put("categoriaUnidadManejo",rs.getString("categoriaUnidadManejo"));
				jo.put("UAC", rs.getString("UAC"));
				jo.put("departamento", rs.getString("departamento"));
				jo.put("categoriaUnidadManejo",rs.getString("categoriaUnidadManejo"));
				jo.put("descripcion", rs.getString("descripcion"));
				jo.put("CAR_Administra", rs.getString("CAR_Administra"));
				jo.put("azimut", rs.getString("azimut"));
				jo.put("fechaInstalacion", rs.getString("fechaInstalacion"));
				jo.put("area", rs.getString("area"));
				jo.put("formaParcela", rs.getString("formaParcela"));
				jo.put("longitud", rs.getString("longitud"));
				jo.put("latitud", rs.getString("latitud"));
				jo.put("tipoFisiografico", rs.getString("tipoFisiografico"));
				jo.put("datosdisponibles", rs.getString("datosdisponibles"));
				jo.put("namestation",rs.getString("namestation"));				

				jsonarrayData.put(jo);

			}
			jsonObject.put("data", jsonarrayData);

		} catch (SQLException ex) {
			Logger.getLogger(getClass().getName()).error(ex.getMessage());

		} catch (Exception ex) {
			Logger.getLogger(getClass().getName()).error(ex.getMessage());

		} finally {
			try {
				con.close();
				con = null;
			} catch (SQLException ex) {
				Logger.getLogger(getClass().getName()).error(ex.getMessage());
			}

		}
		
		return jsonObject;
	}
}
