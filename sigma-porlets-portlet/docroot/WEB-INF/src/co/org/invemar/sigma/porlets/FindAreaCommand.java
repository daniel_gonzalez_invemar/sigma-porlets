package co.org.invemar.sigma.porlets;

import java.sql.Connection;
import java.util.Map;



import co.org.invemar.sigma.porlets.persistence.StudioCase;
import co.org.invemar.conexion.factories.*;

public class FindAreaCommand implements Command{
 
	private RequestCommand requestCommand;
	
	  
	/**
	 * @param requestCommand
	 */
	public FindAreaCommand(RequestCommand requestCommand) {
		super();	
		this.requestCommand = requestCommand;
	}


	@Override
	public String execute() {         
		  
          
      	  ConexionManglar conexionManglar = new ConexionManglar();
          Connection con = conexionManglar.connect(requestCommand.getParameters().get("userDB"),requestCommand.getParameters().get("pwdDB"));  
      	  
      	  Integer codStudioCase = new Integer(requestCommand.getParameters().get("codStudioCase"));      	  
      	
      	  StudioCase studioCase = new StudioCase();
      	  
          String jsonString = studioCase.getAllCaseStudioByType(codStudioCase.intValue(), con).toString();
      	 
		return  jsonString;
	}

}
