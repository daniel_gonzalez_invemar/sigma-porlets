package co.org.invemar.conexion.factories;

import java.sql.Connection;

public interface Conexion {
 public Connection connect(String user, String password);
 public Connection connect(String user,String password, String IP,String scheme);
 
}
