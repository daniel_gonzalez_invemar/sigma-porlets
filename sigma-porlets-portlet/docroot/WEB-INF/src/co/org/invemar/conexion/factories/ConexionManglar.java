package co.org.invemar.conexion.factories;

import java.sql.Connection;
import java.sql.DriverManager;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;



public class ConexionManglar implements Conexion {

	@Override
	public Connection connect(String user, String password) {
	
		Connection con =null;
		con= connect(user, password,"192.168.3.77","sci");
		return con;
	}

	@Override
	public Connection connect(String user, String password, String IP,
			String scheme) {
		 Connection con =null;
		 try {
	            Class.forName("oracle.jdbc.driver.OracleDriver");
	            String url = "jdbc:oracle:thin:@"+IP+":1521:"+scheme+"";	          
	            con = DriverManager.getConnection(url, user, password);
	            

	        } catch (ClassNotFoundException e) {
	            System.out.println("Error connection:" + e.getMessage());

	        } catch (Exception e) {
	            System.out.println("Error connection:" + e.getMessage());
	        }
	        return con;
	}

}
