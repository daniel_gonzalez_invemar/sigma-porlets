<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<portlet:defineObjects />
<portlet:resourceURL var="resourceURL" />

<aui:script>
	AUI().use('aui-tabview', function(Y) {
		var tabview = new Y.TabView({
			srcNode : '#areas'
		}).render();

	});

	LoadTable('parcelas');

	 function setCodKindOfStudioCase(idTableUpdate){
	
		 var codKindOfStudioCase = "";
		 if (idTableUpdate=='sectors'){
			 codKindOfStudioCase=86;
		 }else if (idTableUpdate=='parcelas' ){
			 codKindOfStudioCase=94 ;
		 }
		 
		 return codKindOfStudioCase;
	 }
	
	function LoadTable(idTableUpdate) {

		AUI().use('aui-io-request', function(A) {			
			
			
			A.io.request('<%=resourceURL.toString()%>', {
				method : 'post',
				dataType : 'json',
				data : {
					<portlet:namespace />action : "FINDAREA",
					<portlet:namespace />codStudioCase : setCodKindOfStudioCase(idTableUpdate)
				},
				on : {
					success : function() {
						var json = this.get('responseData');

						if (idTableUpdate == "sectors") {
							conTableSector(json);
						} else if (idTableUpdate == "parcelas") {
							conTableParcelas(json);
						}

					}
				}
			})
		});
	}

	function FormatterDescriptionCell(value, row,index) {
		var shortDescription = value.substr(0, 50);
		var html = "<br/><span style='color:blue;cursor:pointer' onclick=\"activeToogler('"
			+ row.id
			+ "')\">Ver mas...</span><br/><p style='display:none' id='"+row.id+"'>"
			+ value + "<p>";
		if (value=='-'){
		    return '-';	
		}else{
			return html;
		}		
	

	}	

	function activeToogler(idToggler) {
		$("#" + idToggler).toggle();
	}
	
	function conTableParcelas(json){	
	
        var tabla =$('#parcelas').bootstrapTable({
        exportTypes:['csv','excel'],       
		exportDataType:'All',		
		showExport:'true',
		showColumns:'true',
		showFooter:'true',
		searchTimeOut:200,
		escape:'true',
		showToggle:'true',
		search:'true',
		showHeader:'true',
		pagination:'true',
		fixedColumns:'true',
		fixedNumber:2,
		columns:[ {
			field : 'id',
			title : 'Id - Nombre',
			sortable : 'true',
			switchable : 'true',
			searchable : 'true'
		},
		{
			field : 'nombreparcela',
			title : 'Nombre Parcela',
			sortable : 'true',
			switchable : 'true',
			searchable : 'true'
		},
		{
			field : 'namestation',
			title : 'Nombre Estacion',
			sortable : 'true',
			switchable : 'true',
			searchable : 'true'
		},
		{
			field : 'nombresector',
			title : 'nombre Sector',
			sortable : 'true',
			switchable : 'true',
			searchable : 'true'
		},
		{
			field : 'region',
			title : 'Region',
			sortable : 'true',
			switchable : 'true',
			searchable : 'true'
		},
		{
			field : 'departamento',
			title : 'Departamento',
			sortable : 'true',
			switchable : 'true',
			searchable : 'true'
		},
		{
			field : 'UAC',
			title : 'UAC',
			sortable : 'true',
			switchable : 'true',
			searchable : 'true',
			formatter:FormatterDescriptionCell
		},
		{
			field : 'zonaprotegida',
			title : 'Zona Protegida',
			sortable : 'true',
			switchable : 'true',
			searchable : 'true'
		},
		{
			field : 'cuencahidrografica',
			title : 'Cuenca Hidrográfica',
			sortable : 'true',
			switchable : 'true',
			searchable : 'true'
		},
		{
			field : 'descripcion',
			title : 'Descripcion',
			sortable : 'true',
			switchable : 'true',
			searchable : 'true',
			formatter:FormatterDescriptionCell
		},
		{
			field : 'CAR_Administra',
			title : 'CAR Administra',
			sortable : 'true',
			switchable : 'true',
			searchable : 'true'
		},
		{
			field : 'tipounidadmanejo',
			title : 'Tipo Unidad de Manejo',
			sortable : 'true',
			switchable : 'true',
			searchable : 'true'
		},
		{
			field : 'azimut',
			title : 'Azimut',
			sortable : 'true',
			switchable : 'true',
			searchable : 'true'
		},
		{
			field : 'fechaInstalacion',
			title : 'Fecha de Instalacion',
			sortable : 'true',
			switchable : 'true',
			searchable : 'true'
		},
		{
			field : 'area',
			title : 'Area',
			sortable : 'true',
			switchable : 'true',
			searchable : 'true'
		},
		{
			field : 'formaParcela',
			title : 'Forma de Parcela',
			sortable : 'true',
			switchable : 'true',
			searchable : 'true'
		},
		{
			field : 'longitud',
			title : 'Longitud',
			sortable : 'true',
			switchable : 'true',
			searchable : 'true'
		},
		{
			field : 'latitud',
			title : 'Latitud',
			sortable : 'true',
			switchable : 'true',
			searchable : 'true'
		},
		{
			field : 'tipoFisiografico',
			title : 'Tipo Fisiográfico',
			sortable : 'true',
			switchable : 'true',
			searchable : 'true'
		},
		{
			field : 'datosdisponibles',
			title : 'Datos Disponibles',
			sortable : 'true',
			switchable : 'true',
			searchable : 'true'
		}]});
		
	
        
		tabla.bootstrapTable('load', fillRowTablaParcela(json));
		
        
		

	}
	function fillRowTablaParcela(json){
		var rows = [];
		for (var i = 0; i < json.data.length; i++) {

			var data = {
				id : json.data[i].id,
				nombreparcela:json.data[i].nombre,
				nombresector: json.data[i].nombresector,
				nombreestacion:'',
				Region : json.data[i].nombreregion,
				departamento : json.data[i].departamento,
				UAC : json.data[i].UAC,
				zonaprotegida : json.data[i].zonaprotegida,				
				cuencahidrografica : json.data[i].cuencahidrografica,
				descripcion:json.data[i].descripcion,
				CAR_Administra : json.data[i].CAR_Administra,
				tipounidadmanejo:json.data[i].categoriaUnidadManejo,
				azimut:json.data[i].azimut,
				fechaInstalacion:json.data[i].fechaInstalacion,
				area:json.data[i].area,
				longitud:json.data[i].longitud,
				latitud:json.data[i].latitud,
				formaParcela:json.data[i].formaParcela,
				tipoFisiografico:json.data[i].tipoFisiografico,
				datosdisponibles:json.data[i].datosdisponibles,	
				namestation:json.data[i].namestation
				
			};
			rows.push(data);
		}
		if(rows.length>1){
			$('#messageLoadparcelas').hide();
		}
		
	  return rows;
	}
 
	function fillTableSectors(json){
		var rows = [];
		for (var i = 0; i < json.data.length; i++) {

			var data = {
				id : json.data[i].id,
				Nombresector : json.data[i].nombresector,
				Region : json.data[i].nombreregion,
				departamento : json.data[i].departamento,
				UAC : json.data[i].UAC,
				zonaprotegida : json.data[i].zonaprotegida,
				cuencahidrografica : json.data[i].cuencahidrografica,
				Descripcion : json.data[i].descripcion,
				CAR_Administra : json.data[i].CAR_Administra
			};
			rows.push(data);

		}
		
		if(rows.length>1){
			$('#messageLoadsectores').hide();
		}
		
		return rows;
	}
    
	function conTableSector(json) {		

	var tabla=	$('#sectors').bootstrapTable({	
			exportTypes:['csv','excel'],
			exportDataType:'All',
			cardView:'true',
			showExport:'true',
			showColumns : 'true',			
			showFooter : 'true',
			searchTimeOut : 200,
			escape : 'true',
			showToggle : 'true',
			search : 'true',
			showHeader : 'true',
			pagination : 'true',
			columns : [ {
				field : 'id',
				title : 'Id',
				sortable : 'true',
				switchable : 'true',
				searchable : 'true'
			}, {
				field : 'Nombresector',
				title : 'Nombre sector',
				sortable : 'true',
				switchable : 'true',
				searchable : 'true'
			}, {
				field : 'Region',
				title : 'Region',
				sortable : 'true',
				switchable : 'true',
				searchable : 'true'
			}, {
				field : 'departamento',
				title : 'Departamento',
				sortable : 'true',
				switchable : 'true',
				searchable : 'true'
			}, {
				field : 'UAC',
				title : 'UAC',
				sortable : 'true',
				switchable : 'true',
				searchable : 'true',
				formatter:FormatterDescriptionCell
			}, {
				field : 'zonaprotegida',
				title : 'Zona Protegida',
				sortable : 'true',
				switchable : 'true',
				searchable : 'true'
			}, {
				field : 'cuencahidrografica',
				title : 'Cuenca Hidrografica',
				sortable : 'true',
				switchable : 'true',
				searchable : 'true'
			}, {
				field : 'Descripcion',
				title : 'Descripcion',
				sortable : 'true',
				switchable : 'true',
				searchable : 'true',
				formatter:FormatterDescriptionCell				
			}, 
			{
				field : 'CAR_Administra',
				title : 'CAR Administra',
				sortable : 'true',
				switchable : 'true',
				searchable : 'true'
			} ],
			
		});
	     tabla.bootstrapTable('load', fillTableSectors(json));
	}
</aui:script>

<div id="areas">

	<ul class="nav nav-tabs">
	<li><a href="#tab-3" class="active" onclick='LoadTable("parcelas")'>Parcelas</a></li>
	<li ><a href="#tab-1" onclick='LoadTable("sectors")'>Sectores</a></li>		
		
	</ul>

	<div class="tab-content">
	<div id="tab-3" class="tab-pane">		    
		    <a style="display: inline-block;" target="blank" href="http://siam.invemar.org.co/sibm-distribucion-espacial/2239" class="btn btn-lg">
               <span class="glyphicon glyphicon-map-marker"></span> Ver ubicación de parcelas.</a><br/>
                <span id="messageLoadparcelas">Cargando datos...</span>
			<table id="parcelas" >
				
			</table>
		</div>
		<div id="tab-1" class="tab-pane">
		   <span id="messageLoadsectores">Cargando datos...</span>
			<table id="sectors">

			</table>
		</div>		
		
	</div>
	</p>

</div>






